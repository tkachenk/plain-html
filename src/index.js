import './style.scss'
import Vue from 'vue/dist/vue.js';
import get from 'lodash/get'

document.addEventListener('DOMContentLoaded', () => {
  const widget = document.getElementById('inst-widget')
  if (!widget) {
    console.warn('InstWidget. Отсутсвует "#inst-widget"')
    return
  }

  const account = widget.getAttribute('data-account')

  new Vue({
    el: '#inst-widget',
    template: `
    <div class="inst-widget" v-if="!loadError">
      <div class="header">
        <a :href="accountLink" target="_blank">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve">
              <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48h192c61.76,0,112,50.24,112,112V352z" />
            <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z" />
            <circle cx="393.6" cy="118.4" r="17.056" />
          </svg>
          Мы в Instagram
        </a>
      </div>
      <div class="body">
        <div class="gallery" v-if="data">
          <a
            v-for="(item, i) in getValueFromData('edge_owner_to_timeline_media.edges', [])"
            :key="i"
            class="gallery__item"
            :href="'https://www.instagram.com/p/' + get(item, 'node.shortcode', '')"
            target="_blank"
          >
            <img :src="get(item, 'node.thumbnail_resources.1.src', '')" alt="img">  
          </a>
        </div>
        <div class="loader" v-else />
      </div>
    </div>
  `,
    data () {
      return {
        get,
        account: null,
        data: null,
        loadError: false
      }
    },
    computed: {
      accountLink () {
        return `https://www.instagram.com/${account}`
      }
    },
    methods: {
      getValueFromData (str, def = '') {
        if (!str) return null
        return get(this.data, str, def)
      }
    },
    created () {
      this.account = account

      if (this.account) {
        fetch(`https://www.instagram.com/${this.account}/?__a=1`)
          .then((response) => {
            if (response.status < 300) {
              return response.json();
            } else {
              throw new Error('Ошибка загрузки виджета')
            }
          })
          .then(({graphql}) => {
            if (graphql && graphql.user) {
              this.data = graphql.user
            }
          })
          .catch(e => {
            this.loadError = true
            console.error(e)
            console.error('InstWidget. Ошибка загрузки виджета')
          })
      } else {
        console.warn('InstWidget. Не задан account')
      }
    }
  })
})

