<?php
  require_once __DIR__ . '/classes/InstData.php';
  $filename = __DIR__ .'/cache/data.txt';
  $account = $_GET['account'];

  if (!$account) {
    echo 'instwidget: не задан "account"';
    die();
  }

  $cache_hours = 24;
  if ((int)$_GET['cache_hours']) {
    $cache_hours = (int)$_GET['cache_hours'];
  }

  $cache_data = null;
  $data = null;

  if (file_exists($filename)) {
    $now = new DateTime();
    $date = DateTime::createFromFormat('U', filemtime($filename));
    $interval = $now->diff($date);
    if ($interval->h < $cache_hours) {
      $file = file_get_contents($filename);
      $file = json_decode($file);
      if ($file && $file->account === $account) {
        $cache_data = $file;
      }
    }
  }

  if ($cache_data) {
    $data = $cache_data;
  } else {
    $url = 'https://www.instagram.com/' . $account . '/?__a=1';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    $result = json_decode($result);
    curl_close($ch);

    if ($result && isset($result->graphql) && isset($result->graphql->user)) {
      $data = new InstData($result->graphql->user);
      file_put_contents($filename, json_encode($data));
    }
  }

//    echo '<pre>';
//    var_dump($_GET);
//    echo '</pre>';
//    echo '<pre>';
//    var_dump($cache_data);
//    echo '</pre>';
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>InstWidget</title>
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap');

    body {
        margin: 0;
    }

    .widget {
        padding: 10px 0 0;
        border: 1px solid #dbdbdb;
        width: 100%;
        max-width: 255px;
        font-family: "Roboto", sans-serif;
        font-size: 14px;
    }

    .widget > * {
        box-sizing: border-box;
    }

    .header__row {
        padding: 0 10px;
        flex-shrink: 1;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .img__wrap {
        width: 50px;
        height: 50px;
        overflow: hidden;
        border-radius: 50%;
        border: 1px solid #dbdbdb;
        flex-shrink: 0;
        margin-right: 20px;
    }

    .title {
        padding: 0 10px;
        font-weight: 500;
        margin: 20px 0 10px;
        text-align: center;
    }

    .meta {
        display: flex;
        flex-direction: column;
        overflow: hidden;
    }

    .meta__account {
        text-align: center;
        margin: 0 0 8px;
        font-size: 20px;
    }

    .meta__subscribe {
        align-self: center;
        display: inline-block;
        background-color: #0095F6;
        color: #fff;
        padding: 0 12px;
        line-height: 28px;
        outline: none;
        border-radius: 3px;
        font-weight: 500;
        text-decoration: none;
    }

    .statistics {
        padding: 10px;
        border-top: 1px solid #dbdbdb;
        border-bottom: 1px solid #dbdbdb;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .statistics__item {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    .statistics__value {
        margin: 0;
        font-weight: 500;
    }

    .statistics__title {
        font-size: 12px;
        margin: 0 0 8px;
        color: #8E8E8E;
    }

    .gallery {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }

    .gallery__item {
        width: 33.3%;
        padding-top: 33.3%;
        position: relative;
    }

    .gallery__item img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
  </style>
</head>
<body>
  <?php if ($data): ?>
    <div class="widget">
    <div class="header">
      <div class="header__row">
        <div class="img__wrap">
          <img src="<?php echo $data->avatar ?>" alt="img" class="img">
        </div>
        <div class="meta">
          <p class="meta__account">
            <?php echo $account; ?>
          </p>
          <a target="_parent" href="https://www.instagram.com/<?php echo $account ?>" class="meta__subscribe">Подписаться</a>
        </div>
      </div>

      <p class="title">
        <?php echo $data->full_name ?>
      </p>

      <div class="statistics">
        <div class="statistics__item">
          <p class="statistics__title">Публикаций</p>
          <p class="statistics__value">
            <?php echo $data->media_count ?>
          </p>
        </div>
        <div class="statistics__item">
          <p class="statistics__title">Подписчики</p>
          <p class="statistics__value">
            <?php echo $data->followed_by ?>
          </p>
        </div>
        <div class="statistics__item">
          <p class="statistics__title">Подписки</p>
          <p class="statistics__value">
            <?php echo $data->follow ?>
          </p>
        </div>
      </div>
    </div>
    <div class="widget__body">
      <div class="gallery">
        <?php foreach ($data->images as $item): ?>
          <a
            class="gallery__item"
            href="<?php echo $item->link ?>"
            target="_blank"
          >
            <img src="<?php echo $item->src ?>" alt="img">
          </a>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
</body>
</html>

<!--php -S localhost:8000 index.php-->
