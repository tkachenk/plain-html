<?php
require_once __DIR__ . '/Image.php';


class InstData
{
  public $avatar, $full_name, $media_count, $followed_by, $follow, $images, $account;
  function __construct($init)
  {
    $this->account = $this->setValueObject($init, ['username']);
    $this->avatar = $this->setValueObject($init, ['profile_pic_url']);
    $this->full_name = $this->setValueObject($init, ['full_name']);
    $this->media_count = $this->setValueObject($init, ['edge_owner_to_timeline_media', 'count'], 0);
    $this->followed_by = $this->setValueObject($init, ['edge_followed_by', 'count'], 0);
    $this->follow = $this->setValueObject($init, ['edge_follow', 'count'], 0);
    $this->images = $this->setImages($init);
  }

  private function setValueObject($obj, $key_list = [], $default = '') {
    try {
      $result = $obj;
      foreach ($key_list as $key) {
        if (isset($result->{$key})) {
          $result = $result->{$key};
        } else {
          return $default;
        }
      }
      return $result;
    } catch (Exception $e) {
      $e->getMessage();
    }
  }

  private function setImages ($init) {
    $arr = $this->setValueObject($init, ['edge_owner_to_timeline_media', 'edges'], []);
    for ($i = 0; $i < count($arr); $i++) {
      $arr[$i] = new Image($arr[$i]);
    }
    return $arr;
  }
}
