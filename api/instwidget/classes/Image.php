<?php


class Image
{
  public $shortcode, $src, $link;
  function __construct($init)
  {
    $this->shortcode = $this->setValueObject($init, ['node', 'shortcode']);
    $this->link = 'https://www.instagram.com/p/' . $this->shortcode;
    $this->src = $this->setValueObject($init, ['node', 'thumbnail_resources', '0', 'src']);
  }

  private function setValueObject($obj, $key_list = [], $default = '') {
    try {
      $result = $obj;
      foreach ($key_list as $key) {
        if (is_array($result)) {
          if (array_key_exists($key, $result)) {
            $result = $result[$key];
          } else {
            return $default;
          }
        } else {
          if (isset($result->{$key})) {
            $result = $result->{$key};
          } else {
            return $default;
          }
        }
      }
      return $result;
    } catch (Exception $e) {
      $e->getMessage();
    }
  }
}
