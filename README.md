## Instagram widget

#### Getting Started

##### development
* yarn
* yarn dev

##### for build
* yarn build

after the build, the index file is updated in "public" dir

### Example usage
```html
<div id="inst-widget" data-account="{account}"></div>
<script src="{pathToIndex}/index.js"></script>
```
{account} - is displayed account

{index.js} - file js from public directory

